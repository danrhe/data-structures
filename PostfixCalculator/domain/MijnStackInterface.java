package domain;

import java.util.EmptyStackException;

/**
 * Interface voor een abstracte datatype dat elementen volgens het First In - Last Out principe elementen verzameld.
 */
public interface MijnStackInterface<E> {
    /**
     * Leegt een nieuw element op de stapel
     *
     * @param e het element van type E dat op de stapel gelegt wordt
     */
    public void push(E e);

    /**
     * Verwijdert het bovenste element van de stapel
     * @return het te verwijderen element
     * @throws EmptyStackException als stapel leeg is.
     */
    public E pop() throws EmptyStackException;

    /**
     * Levert het aantal elementen op de stapel
     */
    public int size();

    /**
     * Is true als geen elementen op de stapel liggen, anders false
     */
    public boolean isEmpty();

    /**
     * Levert het element aan de top van de stapel
     */
    public E top();
}
