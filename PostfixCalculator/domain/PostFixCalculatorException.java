package domain;


public class PostFixCalculatorException extends Exception{
    public PostFixCalculatorException() {
        super();
    }

    public PostFixCalculatorException(String message) {
        super(message);
    }
}
