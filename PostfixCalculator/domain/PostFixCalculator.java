package domain;


public class PostFixCalculator {

    private PostFixCalculator(){}

    /**
     * Doet een berekening volgens de postfix notatie
     *
     * @param expressie De string met de postfix expressie.
     *
     * @return Het resultaat van de berekening.
     *
     * @throws PostFixCalculatorException Als expressie niet valide is.
     */
    public static long calculate(String expressie) throws PostFixCalculatorException{

        valideerExpressieContent(expressie);

        MijnStack<Long> getallen = new MijnStack<Long>();

        TokenLezer lezer = new TokenLezer(expressie);
        Token token = lezer.volgendeToken();

        while (token != null){

            int type = token.getType();
            if (type == Token.GETAL){
                getallen.push(Long.parseLong(token.getInhoud()));

            } else {
                long eersteGetal;
                long tweedeGetal;
                if (getallen.size() > 1){
                    eersteGetal = getallen.pop();
                    tweedeGetal = getallen.pop();
                    Long result = null;

                    switch (type) {
                        case Token.DELING:
                            if (eersteGetal == 0){
                                throw new PostFixCalculatorException("Door nul delen is niet toegestaan.");
                            }
                            if (tweedeGetal % eersteGetal != 0){
                                throw new PostFixCalculatorException("Deling levert geen geheel getal op.");
                            }
                            result = tweedeGetal / eersteGetal;
                            break;

                        case Token.MAAL:
                            if (Long.MAX_VALUE / eersteGetal < Math.abs(tweedeGetal) ){
                                throw new PostFixCalculatorException("Vermenigvuldiging levert te groot/klein getal op.");
                            }
                            result = tweedeGetal * eersteGetal;
                            break;

                        case Token.MIN:
                            if (Long.MIN_VALUE + Math.abs(eersteGetal) > Math.abs(tweedeGetal)){
                                throw new PostFixCalculatorException("Substractie levert te klein getal op.");
                            }
                            result = tweedeGetal - eersteGetal;
                            break;

                        case Token.PLUS:
                            if (Long.MAX_VALUE - Math.abs(eersteGetal) < Math.abs(tweedeGetal)){
                                throw new PostFixCalculatorException("Optellen levert te groot/klein getal op.");
                            }
                            result = tweedeGetal + eersteGetal;
                            break;
                    }

                    getallen.push(result);
                } else {
                    throw new PostFixCalculatorException("Er zijn te veel operatoren of ze zijn verkeerd geplaatst");
                }
            }
            token = lezer.volgendeToken();
        }

        if (getallen.size() > 1) {
            throw new PostFixCalculatorException("Expressie bevat niet genoeg operatoren");
        }

        return getallen.pop();

    }

    /**
     * Valideert of een string de juiste tekens bevat om verwerkt te worden.
     *
     * @param expressie de expressie die te testen is.
     *
     * @throws PostFixCalculatorException Als de meegeleverde expressie null of leeg is of niet toegestaande tekens bevat.
     */
    private static void valideerExpressieContent(String expressie) throws PostFixCalculatorException{

        if (expressie == null || expressie.equals("")){
            throw new PostFixCalculatorException("Expressie is leeg");
        }

        String pattern = "[\\d\\s\\+\\-\\*\\/]+";

        if(!expressie.matches(pattern)){
            throw new PostFixCalculatorException("Expressie bevat verboden characters. Toegestaan zijn " +
                    "gehele getallen, +, - , *, / en spaties");
        }


    }

}
