package domain;


import java.util.EmptyStackException;
import java.util.Vector;

public class MijnStack<E> implements MijnStackInterface<E> {

    private Vector<E> vector = null;

    public MijnStack() {
        vector = new Vector<E>();
    }

    @Override
    public void push(E e) {
        vector.add(e);
    }

    @Override
    public E pop() throws EmptyStackException {
        if (size() == 0) {
            throw new EmptyStackException();
        }
        E e = vector.elementAt(size() - 1);
        vector.removeElementAt(size() - 1);
        return e;

    }

    @Override
    public int size() {
        return vector.size();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public E top() throws EmptyStackException {
        if (size() == 0) {
            throw new EmptyStackException();
        }

        return vector.elementAt(size() - 1);

    }
}