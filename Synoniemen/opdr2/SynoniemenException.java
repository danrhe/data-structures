package opdr2;

public class SynoniemenException extends Exception {
    public SynoniemenException() {
    }

    public SynoniemenException(String message) {
        super(message);
    }
}
