package opdr2;

import java.util.*;

public class SynoniemenBeheer {

    /*
     * Comparator klasse die String objecten eerst op lengte en vervolgens alfanumeriek vergelijkt.
     */
    static class LengthAlphaComparator implements Comparator<String> {
        @Override
        public int compare(String s1, String s2) {
            if (s1.length() < s2.length()){
                return -1;
            }
            if (s1.length() > s2.length()){
                return 1;
            }

            return s1.compareTo(s2);
        }
    }

    private TreeMap<String, SortedSet<String>> synoniemen;

    public SynoniemenBeheer(){
        synoniemen = new TreeMap<>();
    }

    /**
     * Voegt een woord synoniemen combinatie aan de collectie toe.
     *
     * @param woord het woord waarvoor synoniemen gezocht worden
     *
     * @param synoniemenString Een string met synoniemen gescheiden door een spatie
     */
    public void add(String woord, String synoniemenString){

        SortedSet<String> synoniemen = new TreeSet<>(new LengthAlphaComparator());
        synoniemen.addAll(Arrays.asList(synoniemenString.split(" ")));

        this.synoniemen.put(woord, synoniemen);
    }


    /**
     * Haalt alle synoniemen voor een bepaald woord op.
     *
     * @param woord Het woord waarvoor synoniemen gezocht worden
     *
     * @return De synoniemen voor het woord
     *
     * @throws SynoniemenException als sleutelwoord null is of niet in TreeMap is opgenomen
     */
    public String[] getSynoniemen(String woord) throws SynoniemenException{

        if (woord == null){
            throw new SynoniemenException("Waarde voor woord is null");
        }
        if (!synoniemen.containsKey(woord)){
            throw new SynoniemenException("Collectie bevat geen synoniemen voor het gezochte woord (" + woord + ")");
        }

        SortedSet<String> found = synoniemen.get(woord);

        return (found.toArray(new String[0]));
    }


    /**
     * Haalt alle opgeslagen woorden op.
     *
     */
    public String[] getWoorden(){

        Set<String> set = synoniemen.keySet();

        return set.toArray(new String[0]);
    }

}